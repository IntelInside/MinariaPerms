package net.minaria.permissions.sql;

import net.minaria.permissions.RankEnum;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.UUID;

public class SQLUtils {
    public static HashMap<Player, RankEnum> rankEnumHashMap = new HashMap<>();

    protected static MySQL sql;
    protected static Connection c;

    public void setupSQL(String hostname, String port, String database, String username, String password) throws SQLException, ClassNotFoundException {
        sql = new MySQL(hostname, port, database, username, password);
        c = sql.openConnection();
        Statement s = c.createStatement();
        s.executeUpdate("CREATE TABLE IF NOT EXISTS `min_ranks` (" +
                " `ID` INT NOT NULL AUTO_INCREMENT," +
                " `UUID` VARCHAR(36) NOT NULL," +
                " `RANK` MEDIUMTEXT CHARACTER SET 'utf8' NOT NULL," +
                " PRIMARY KEY (`ID`));");
    }

    public RankEnum getRank(UUID uuid) throws SQLException, ClassNotFoundException {
        String rank = "DEFAULT";
        Statement s = c.createStatement();
        ResultSet rs = s.executeQuery("SELECT * FROM `min_ranks` WHERE UUID='" + uuid + "';");
        if (!rs.next()) {
            return null;
        } else {
            rs = s.executeQuery("SELECT * FROM `min_ranks` WHERE UUID='" + uuid + "';");
            while (rs.next()) {
                rank = rs.getString("RANK");
            }
            if (rank.equalsIgnoreCase("owner")) {
                return RankEnum.OWNER;
            }
            if (rank.equalsIgnoreCase("admin")) {
                return RankEnum.ADMIN;
            }
            if (rank.equalsIgnoreCase("srmod")) {
                return RankEnum.SRMOD;
            }
            if (rank.equalsIgnoreCase("mod")) {
                return RankEnum.MOD;
            }
            if (rank.equalsIgnoreCase("helper")) {
                return RankEnum.HELPER;
            }
            if (rank.equalsIgnoreCase("jrdev")) {
                return RankEnum.JRDEV;
            }
            if (rank.equalsIgnoreCase("dev")) {
                return RankEnum.DEV;
            }
            if (rank.equalsIgnoreCase("default")) {
                return RankEnum.DEFAULT;
            }
            if (rank.equalsIgnoreCase("ultra")) {
                return RankEnum.ULTRA;
            }
            if (rank.equalsIgnoreCase("mega")) {
                return RankEnum.MEGA;
            }
            if (rank.equalsIgnoreCase("grand")) {
                return RankEnum.GRAND;
            }
            if (rank.equalsIgnoreCase("builder")) {
                return RankEnum.BUILDER;
            }
            return null;
        }
    }

    public void setRank(UUID uuid, RankEnum rank, boolean newEntry) throws SQLException, ClassNotFoundException {
        Statement s = c.createStatement();
        if (!newEntry) {
            if (rank.equals(RankEnum.DEFAULT)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='DEFAULT' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.HELPER)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='HELPER' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.MOD)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='MOD' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.SRMOD)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='SRMOD' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.ADMIN)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='ADMIN' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.JRDEV)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='JRDEV' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.DEV)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='DEV' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.OWNER)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='OWNER' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.ULTRA)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='ULTRA' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.MEGA)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='MEGA' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.GRAND)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='GRAND' WHERE UUID='" + uuid + "';");
            }
            if (rank.equals(RankEnum.BUILDER)) {
                s.executeUpdate("UPDATE `min_ranks` SET RANK='BUILDER' WHERE UUID='" + uuid + "';");
            }
        }
        if (newEntry) {
            s.executeUpdate("INSERT IGNORE INTO `min_ranks` (UUID, RANK) VALUES ('" + uuid + "', 'DEFAULT')");
        }
    }
}
