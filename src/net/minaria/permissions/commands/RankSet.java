package net.minaria.permissions.commands;

import net.minaria.permissions.Main;
import net.minaria.permissions.RankEnum;
import net.minaria.permissions.sql.SQLUtils;
import net.minaria.permissions.util.UUIDFetcher;
import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.EnumTitleAction;
import net.minecraft.server.v1_8_R1.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class RankSet implements CommandExecutor {
    private Main plugin;
    private SQLUtils sql = new SQLUtils();

    public RankSet(Main main) {
        plugin = main;
    }

    private void setRank(String rank, String name, CommandSender sender) {
        try {
            RankEnum newRank = RankEnum.valueOf(rank.toUpperCase());
            if (newRank == null) {
                sender.sendMessage(ChatColor.DARK_AQUA + "Permissions " + ChatColor.DARK_GRAY + ">> " + ChatColor.GRAY + "You have entered an invalid rank. Please revise your syntax.");
            }
            sql.setRank(UUIDFetcher.getUUIDOf(name), newRank, false);
            PacketPlayOutTitle ppot = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, ChatSerializer.a("[{\"text\":\"Your rank has been updated to \",\"color\":\"gray\"},{\"extra\":[\"text\":\"" + newRank.name() + "\",\"color\":\"green\"]}]"));
            ((CraftPlayer) Bukkit.getPlayer(name)).getHandle().playerConnection.sendPacket(ppot);
        } catch (Exception e) {
            sender.sendMessage(ChatColor.DARK_AQUA + "Permissions " + ChatColor.DARK_GRAY + ">> " + ChatColor.GRAY + "The Java Interpreter has failed to execute the operation. The message passed on is '" + ChatColor.GREEN + e.getMessage() + ChatColor.GRAY + "'.");
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("rankset")) {
            if (commandSender instanceof ConsoleCommandSender) {
                if (strings.length == 2) {
                    setRank(strings[1], strings[0], commandSender);
                } else {
                    commandSender.sendMessage(ChatColor.DARK_AQUA + "Syntax " + ChatColor.DARK_GRAY + ">> " + ChatColor.GREEN + "/rankset <player> <rank>");
                    commandSender.sendMessage(ChatColor.DARK_AQUA + "Syntax " + ChatColor.DARK_GRAY + ">> " + ChatColor.GRAY + "Available ranks are " + ChatColor.GREEN + "owner dev admin jrdev srmod mod helper builder grand mega ultra default" + ChatColor.GRAY + ".");
                }
            }
            if (commandSender instanceof Player) {
                RankEnum rank = null;
                try {
                    rank = sql.getRank(UUIDFetcher.getUUIDOf(commandSender.getName()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (RankEnum.getHigherRanks(RankEnum.ADMIN).contains(SQLUtils.rankEnumHashMap.get(commandSender))) {
                    if (strings.length == 2) {
                        setRank(strings[1], strings[0], commandSender);
                    } else {
                        commandSender.sendMessage(ChatColor.DARK_AQUA + "Syntax " + ChatColor.DARK_GRAY + ">> " + ChatColor.GREEN + "/rankset <player> <rank>");
                        commandSender.sendMessage(ChatColor.DARK_AQUA + "Syntax " + ChatColor.DARK_GRAY + ">> " + ChatColor.GRAY + "Available ranks are " + ChatColor.GREEN + "owner dev admin jrdev srmod mod helper builder grand mega ultra default" + ChatColor.GRAY + ".");
                    }
                } else {
                    commandSender.sendMessage(ChatColor.DARK_AQUA + "Permissions " + ChatColor.DARK_GRAY + ">> " + ChatColor.GRAY + "You must be of rank " + ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "ADMIN" + ChatColor.DARK_GRAY + "]" + ChatColor.GRAY + " or higher to perform this command.");
                }
            }
        }
        return false;
    }
}
