package net.minaria.permissions.listeners;

import net.minaria.permissions.Main;
import net.minaria.permissions.RankEnum;
import net.minaria.permissions.sql.SQLUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class OnPlayerChat implements Listener {
    private Main plugin;
    private SQLUtils sql = new SQLUtils();

    public OnPlayerChat(Main main) {
        plugin = main;
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) throws Exception {
        String message = e.getMessage();
        e.setCancelled(true);
        Bukkit.broadcastMessage(RankEnum.getRankPrefix(SQLUtils.rankEnumHashMap.get(e.getPlayer())) + ChatColor.RESET + ChatColor.YELLOW + e.getPlayer().getName() + ChatColor.WHITE + " " + message);
    }
}
