package net.minaria.permissions.listeners;

import net.minaria.permissions.Main;
import net.minaria.permissions.RankEnum;
import net.minaria.permissions.sql.SQLUtils;
import net.minaria.permissions.util.UUIDFetcher;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class OnPlayerJoin implements Listener {
    private Main plugin;
    private boolean existsInDB = true;

    private SQLUtils sql = new SQLUtils();

    public OnPlayerJoin(Main main) {
        plugin = main;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) throws Exception {
        if (sql.getRank(UUIDFetcher.getUUIDOf(e.getPlayer().getName())) == null) {
            sql.setRank(UUIDFetcher.getUUIDOf(e.getPlayer().getName()), RankEnum.DEFAULT, true);
            SQLUtils.rankEnumHashMap.put(e.getPlayer(), RankEnum.DEFAULT);
        } else {
            SQLUtils.rankEnumHashMap.put(e.getPlayer(), sql.getRank(UUIDFetcher.getUUIDOf(e.getPlayer().getName())));
        }
    }
}
