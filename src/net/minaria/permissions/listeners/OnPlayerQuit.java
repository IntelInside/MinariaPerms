package net.minaria.permissions.listeners;

import net.minaria.permissions.Main;
import net.minaria.permissions.sql.SQLUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class OnPlayerQuit implements Listener {
    private Main plugin;
    private SQLUtils sql = new SQLUtils();

    public OnPlayerQuit(Main main) {
        plugin = main;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        SQLUtils.rankEnumHashMap.remove(e.getPlayer());
    }
}
