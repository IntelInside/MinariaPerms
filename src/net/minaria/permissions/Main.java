package net.minaria.permissions;

import net.minaria.permissions.commands.RankSet;
import net.minaria.permissions.listeners.OnPlayerChat;
import net.minaria.permissions.listeners.OnPlayerJoin;
import net.minaria.permissions.listeners.OnPlayerQuit;
import net.minaria.permissions.sql.SQLUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

public class Main extends JavaPlugin {
    public Logger logger = Bukkit.getLogger();

    FileConfiguration config;
    File file;

    private SQLUtils sql = new SQLUtils();

    @Override
    public void onEnable() {
        config = getConfig();
        file = new File(getDataFolder(), "config.yml");
        if (file.exists() == false) {
            config.options().copyDefaults(true);
            try {
                config.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            sql.setupSQL(config.get("mysql_host").toString(), config.get("mysql_port").toString(), config.get("mysql_db").toString(), config.get("mysql_user").toString(), config.get("mysql_pass").toString());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        getCommand("rankset").setExecutor(new RankSet(this));
        Bukkit.getPluginManager().registerEvents(new OnPlayerQuit(this), this);
        Bukkit.getPluginManager().registerEvents(new OnPlayerJoin(this), this);
        Bukkit.getPluginManager().registerEvents(new OnPlayerChat(this), this);
    }
}
