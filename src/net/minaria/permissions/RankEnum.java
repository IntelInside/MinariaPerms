package net.minaria.permissions;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.HashMap;

public enum RankEnum {
    OWNER,
    DEV,
    JRDEV,
    ADMIN,
    SRMOD,
    MOD,
    HELPER,
    BUILDER,
    GRAND,
    MEGA,
    ULTRA,
    DEFAULT;

    public static String getRankPrefix(RankEnum rank) {
        final HashMap<RankEnum, String> prefix = new HashMap<RankEnum, String>() {{
            put(DEFAULT, ChatColor.GRAY + "");
            put(HELPER, ChatColor.DARK_AQUA + "HELPER ");
            put(MOD, ChatColor.GREEN + "MOD ");
            put(SRMOD, ChatColor.GREEN + "SRMOD ");
            put(ADMIN, ChatColor.DARK_PURPLE + "ADMIN ");
            put(JRDEV, ChatColor.DARK_PURPLE + "JRDEV ");
            put(DEV, ChatColor.DARK_RED + "DEV ");
            put(OWNER, ChatColor.DARK_RED + "OWNER ");
            put(ULTRA, ChatColor.GREEN + "ULTRA ");
            put(MEGA, ChatColor.RED + "MEGA ");
            put(GRAND, ChatColor.LIGHT_PURPLE + "GRAND ");
            put(BUILDER, ChatColor.DARK_GREEN + "BUILDER ");
        }};

        return prefix.get(rank);
    }

    public static ArrayList<RankEnum> getHigherRanks(RankEnum lowestRank) {
        ArrayList<RankEnum> ranks = new ArrayList<>();
        for (RankEnum rank : RankEnum.values()) {
            if (rank != lowestRank) {
                ranks.add(rank);
            } else {
                return ranks;
            }
        }
        return ranks;
    }
}
